                                                                                                                                                                                                                                                                                                                                                                       JUC笔记



# 一、JUC是什么

## 01. java.util.concurrent在并发编程中使用的工具类

![image-20210614003541426](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210614003541426.png)

## 02. 进程/线程回顾

### 01. 进程/线程是什么？

进程：进程是一个具有一定独立功能的程序关于某个数据集合的一次运行活动。它是操作系统动态执行的基本单元，在传统的操作系统中，进程既是基本的分配单元，也是基本的执行单元。

线程：通常在一个进程中可以包含若干个线程，当然一个进程中至少有一个线程，不然没有存在的意义。线程可以利用进程所拥有的资源，在引入线程的操作系统中，通常都是把进程作为分配资源的基本单位，而把线程作为独立运行和独立调度的基本单位，由于线程比进程更小，基本上不拥有系统资源，故对它的调度所付出的开销就会小得多，能更高效的提高系统多个程序间并发执行的程度。

### 02. 进程/线程例子


 使用QQ，查看进程一定有一个QQ.exe的进程，我可以用qq和A文字聊天，和B视频聊天，给C传文件，给D发一段语言，QQ支持录入信息的搜索。

大四的时候写论文，用word写论文，同时用QQ音乐放音乐，同时用QQ聊天，多个进程。

word如没有保存，停电关机，再通电后打开word可以恢复之前未保存的文档，word也会检查你的拼写，两个线程：容灾备份，语法检查。

### 03. 线程状态？

```java
Thread.State
 
 
 
public enum State {
    /**
     * Thread state for a thread which has not yet started.
     */
    NEW,(新建)

    /**
     * Thread state for a runnable thread.  A thread in the runnable
     * state is executing in the Java virtual machine but it may
     * be waiting for other resources from the operating system
     * such as processor.
     */
    RUNNABLE,（准备就绪）

    /**
     * Thread state for a thread blocked waiting for a monitor lock.
     * A thread in the blocked state is waiting for a monitor lock
     * to enter a synchronized block/method or
     * reenter a synchronized block/method after calling
     * {@link Object#wait() Object.wait}.
     */
    BLOCKED,（阻塞）

    /**
     * Thread state for a waiting thread.
     * A thread is in the waiting state due to calling one of the
     * following methods:
     * <ul>
     *   <li>{@link Object#wait() Object.wait} with no timeout</li>
     *   <li>{@link #join() Thread.join} with no timeout</li>
     *   <li>{@link LockSupport#park() LockSupport.park}</li>
     * </ul>
     *
     * <p>A thread in the waiting state is waiting for another thread to
     * perform a particular action.
     *
     * For example, a thread that has called <tt>Object.wait()</tt>
     * on an object is waiting for another thread to call
     * <tt>Object.notify()</tt> or <tt>Object.notifyAll()</tt> on
     * that object. A thread that has called <tt>Thread.join()</tt>
     * is waiting for a specified thread to terminate.
     */
    WAITING,（不见不散）

    /**
     * Thread state for a waiting thread with a specified waiting time.
     * A thread is in the timed waiting state due to calling one of
     * the following methods with a specified positive waiting time:
     * <ul>
     *   <li>{@link #sleep Thread.sleep}</li>
     *   <li>{@link Object#wait(long) Object.wait} with timeout</li>
     *   <li>{@link #join(long) Thread.join} with timeout</li>
     *   <li>{@link LockSupport#parkNanos LockSupport.parkNanos}</li>
     *   <li>{@link LockSupport#parkUntil LockSupport.parkUntil}</li>
     * </ul>
     */
    TIMED_WAITING,（过时不候）

    /**
     * Thread state for a terminated thread.
     * The thread has completed execution.
     */
    TERMINATED;(终结)
}
```

### 04. wait/sleep的区别？

wait/sleep
功能都是当前线程暂停，有什么区别？
wait放开手去睡，放开手里的锁
sleep握紧手去睡，醒了手里还有锁

 ### 05. 什么是并发？什么是并行？

并发：同一时刻多个线程在访问同一个资源，多个线程对一个点
      例子：小米9今天上午10点，限量抢购
            春运抢票
            电商秒杀...
并行：多项工作一起执行，之后再汇总
      例子：泡方便面，电水壶烧水，一边撕调料倒入桶中



# 二、Lock接口

## 01. 复习Synchronized

多线程编程模板：

* 线程 操作 资源类
* 高内聚低耦合

实现步骤：

* 创建资源类
* 资源类里创建同步方法、同步代码块

例子卖票程序：

SaleTicket.java

## 02. Lock

**是什么:**

![image-20210614175455429](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210614175455429.png)

**Lock接口的实现：**
		ReentrantLock可重入锁

![image-20210614175534048](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210614175534048.png)

**补充：** [一文彻底理解ReentrantLock可重入锁的使用](https://baijiahao.baidu.com/s?id=1648624077736116382&wfr=spider&for=pc)

**如何使用：**

```java
class X {
   private final ReentrantLock lock = new ReentrantLock();
   // ...
 
   public void m() {
     lock.lock();  // block until condition holds
     try {
       // ... method body
     } finally {
       lock.unlock()
     }
   }
 }
```

**synchronized与Lock的区别** 

两者区别：

1. 首先synchronized是java内置关键字，在jvm层面，Lock是个java类；
2. synchronized无法判断是否获取锁的状态，Lock可以判断是否获取到锁；
3. synchronized会自动释放锁(a 线程执行完同步代码会释放锁 ；b 线程执行过程中发生异常会释放锁)，Lock需在finally中手工释放锁（unlock()方法释放锁），否则容易造成线程死锁；
4. 用synchronized关键字的两个线程1和线程2，如果当前线程1获得锁，线程2线程等待。如果线程1阻塞，线程2则会一直等待下去，而Lock锁就不一定会等待下去，如果尝试获取不到锁，线程可以不用一直等待就结束了；
5. synchronized的锁可重入、不可中断、非公平，而Lock锁可重入、可判断、可公平（两者皆可）
6. Lock锁适合大量同步的代码的同步问题，synchronized锁适合代码少量的同步问题。



**创建线程方式：**

1. 继承Thread

   `public class SaleTicket extends Thread`

   java是单继承，资源宝贵，要用接口方式

2. new Thread()

   ```java
   Thread t1 = new Thread();
      t1.start();
   ```

3. 第三种

   ```java
   Thread t1 = new Thread(Runnable target, String name)
   ```

   

**实现runnable方法:**

1. 新建类实现runnable接口:

```java
class MyThread implements Runnable//新建类实现runnable接口
 
new Thread(new MyThread,...)
```

​		这种方法会新增类，有更新更好的方法

2. 匿名内部类

```java
new Thread(new Runnable() {
    @Override
    public void run() {
 
    }
   }, "your thread name").start();
```

 		这种方法不需要创建新的类，可以new接口

2. lambda表达式

```java
new Thread(() -> {
 
 }, "your thread name").start();
```

​		这种方法代码更简洁精炼



**例子卖票程序代码：**

​		使用匿名内部类方式

```java
/**
 * SaleTicket
 * 题目：三个售票员卖出30张票
 * 多线程编程的及业绩套路+模板
 * 1. 在高内聚低耦合的前提下，线程     操作（对外暴露的调用方法）  资源类
 *
 * @author daixl
 * @date 2021/6/14
 */
public class SaleTicket {

    public static void main(String[] args) {
        Ticket ticket = new Ticket();

        //匿名内部类
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 40; i++) {
                    ticket.saleTicket();
                }
            }
        }, "A").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 40; i++) {
                    ticket.saleTicket();
                }
            }
        }, "B").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 40; i++) {
                    ticket.saleTicket();
                }
            }
        }, "C").start();
    }
}

/**
 * 资源类
 */
class Ticket {

    private int number = 30;
    private Lock lock = new ReentrantLock();

    public void saleTicket(){
        lock.lock();
        try {
            if (number > 0) {
                System.out.println(Thread.currentThread().getName() + "\t卖出第：" + (number --) + "\t还剩下：" + number);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
```



​		使用正则表达式例子：

```java
/**
 * SaleTicket
 * 题目：三个售票员卖出30张票
 * 多线程编程的及业绩套路+模板
 * 1. 在高内聚低耦合的前提下，线程     操作（对外暴露的调用方法）  资源类
 *
 * @author daixl
 * @date 2021/6/14
 */
public class SaleTicket {

    public static void main(String[] args) {
        Ticket ticket = new Ticket();
        
        /**
         * 使用lambda表达式写法
         */
        new Thread(() -> {for (int i = 1; i <= 40; i++) ticket.saleTicket();}, "A").start();
        new Thread(() -> {for (int i = 1; i <= 40; i++) ticket.saleTicket();}, "B").start();
        new Thread(() -> {for (int i = 1; i <= 40; i++) ticket.saleTicket();}, "C").start();
    }
}

/**
 * 资源类
 */
class Ticket {

    private int number = 30;
    private Lock lock = new ReentrantLock();

    public void saleTicket(){
        lock.lock();
        try {
            if (number > 0) {
                System.out.println(Thread.currentThread().getName() + "\t卖出第：" + (number --) + "\t还剩下：" + number);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
```



# 三、Java8之lambda表达式复习

## 01. lambda表达式

**什么是Lambda：**

​		Lambda 是一个匿名函数，我们可以把 Lambda表达式理解为是一段可以传递的代码（将代码像数据一样进行传递）。可以写出更简洁、更灵活的代码。作为一种更紧凑的代码风格，使Java的语言表达能力得到了提升。

​		Lambda 表达式在Java 语言中引入了一个新的语法元素和操作符。这个操作符为 “->” ， 该操作符被称为 Lambda 操作符或剪头操作符。它将 Lambda 分为
两个部分：

* 左侧：指定了 Lambda 表达式需要的所有参数
* 右侧：指定了 Lambda 体，即 Lambda 表达式要执行的功能



**要求：**

​		接口只有一个方法

​		lambda表达式，如果一个接口只有一个方法，我可以把方法名省略，即为：即为函数式接口

```java
Foo foo = () -> {System.out.println("****hello lambda");};
```

**写法：**

​		拷贝小括号（），写死右箭头->，落地大括号{...}

**函数式接口：**

​		lambda表达式，必须是函数式接口，必须只有一个方法
​		如果接口只有一个方法java默认它为函数式接口。
​		为了正确使用Lambda表达式，需要给接口加个注解：@FunctionalInterface
​		如有两个方法，立刻报错

​		Runnable接口为什么可以用lambda表达式？

 ![image-20210614190727879](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210614190727879.png)

## 02. 接口里是否能有实现方法？

可以有

**default方法：**

​		接口里在java8后容许有接口的实现，default方法默认实现

```java
default int div(int x,int y) {
  return x/y;
 }
```

接口里default方法可以有几个？   多个

**静态方法实现：**

​		静态方法实现：接口新增 

```
public static int sub(int x,int y){
  return x-y;
}
```

可以有几个？  多个
		注意静态的叫类方法，能用foo去调吗？要改成Foo

## 03. 代码

```java
package lut.juc;

/**
 * LambdaExpress，不关心方法名
 * 口诀：拷贝小括号，写死右箭头，落地大括号
 * 接口里面有且只有一个方法，就可以使用，它是函数式接口
 */
@FunctionalInterface//函数式接口注解
interface Foo1{
    public void sayHello();
}

@FunctionalInterface//函数式接口注解
interface Foo2{
    public int add(int x,int y);
}

@FunctionalInterface//函数式接口注解
interface Foo3{
    public int add(int x,int y);

    //函数式接口可以有多个 default实现
    //JDK1.8之后 接口里面可以有方法的实现
    default int div(int x,int y){
        System.out.println(x+"/"+y+"="+(x/y));
        return x/y;
    };
    default int mv(int x,int y){
        System.out.println(x+"*"+y+"="+(x*y));
        return x*y;
    };
    //可以有多个静态函数
    static int div2(int x,int y){
        System.out.println(x+"/"+y+"="+(x/y));
        return x/y;
    };
    static int mv2(int x,int y){
        System.out.println(x+"*"+y+"="+(x*y));
        return x*y;
    };
}



public class demo2 {
    public static void main(String[] args) {
//        Foo foo=new Foo() {
//            @Override
//            public void sayHello() {
//                System.out.println("Hello");
//            }
//        };
//        foo.sayHello();

        Foo1 foo1=()->{ System.out.println("hello"); };
        foo1.sayHello();

        //Foo2 foo2=(int x,int y)->{
        Foo2 foo2=(x,y)->{
            System.out.println(x+"+"+y+"="+(x+y));
            return x*y;
        };
        foo2.add(10,5);

        Foo3 foo3=(x,y)->{
            System.out.println(x+"+"+y+"="+(x+y));
            return x*y;
        };
        System.out.println(foo3.add(10,5));
        System.out.println(foo3.div(10,5));
        System.out.println(foo3.mv(10,5));
        System.out.println(Foo3.div2(10,5));
        System.out.println(Foo3.mv2(10,5));

    }
}
```

**扩展：** [【Java基础】Java8新特性—接口中使用default和static关键字](https://blog.csdn.net/qq877728715/article/details/104862860)

```apl
Java 1.8对接口有两个方面的增强：接口中可以添加使用default或者static修饰的方法

增加default方法：又叫做接口扩展方法，即在不破坏java现有实现架构的情况下能往接口里增加新方法， default关键字可以给接口添加一个非抽象的方法实现，子类可以直接调用！

增加static方法: 接口中用static修饰的方法也可以有方法体，和类的静态方法一样，可以通过 接口名.方法名 进行接口中 static方法的调用。
```





# 四、线程间通信

## 01. 面试题：两个线程打印

现在两个线程，可以操作初始值为零的一个变量，
实现一个线程对该变量加1，一个线程对该变量-1，
实现交替，来10轮，变量初始值为0.

==要求用线程间通信。==

## 02. 多线程编程模板中

* 判断
* 干活
* 通知

## 03. synchronized实现

```JAVA
/**
 * ThreadWaitNotifyDemo     synchronized版
 * 题目：现在两个线程，可以操作初始值为零的一个变量，
 * 实现一个线程对该变量加1，一个线程对该变量-1，
 * 实现交替，来10轮，变量初始值为0.
 * 1. 高内聚低耦合前提下，线程操作资源类
 * 2. 交互中，判断/干活/通知
 * 3. 多线程交互中，防止虚假唤醒(判断只能用while，不能用if)
 * 知识小总结：多线程编程套路+while判断+新版写法
 * @author daixl
 * @date 2021/6/14
 */
public class ThreadWaitNotifyDemo1 {

    public static void main(String[] args) {
        AirConditioner1 airConditioner = new AirConditioner1();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    airConditioner.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "A").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    airConditioner.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "B").start();
    }
}

/**
 * 资源类
 */
class AirConditioner1 {

    private int number = 0;

    public synchronized void increment() throws InterruptedException {
        //1.判断
        if (number != 0){
            this.wait();
        }
        while (number != 0){
            this.wait();
        }
        //2.干活
        number++;
        System.out.println(Thread.currentThread().getName() + "\t" + number);
        //3.通知
        this.notifyAll();
    }

    public synchronized void decrement() throws InterruptedException {
        //1.判断
        if (number == 0){
            this.wait();
        }
        //2.干活
        number--;
        System.out.println(Thread.currentThread().getName() + "\t" + number);
        //3.通知
        this.notifyAll();
    }
}
```

**说明：**

```apl
如果对象调用了wait方法就会使持有该对象的线程把该对象的控制权交出去，然后处于等待状态。

如果对象调用了notify方法就会通知某个正在等待这个对象的控制权的线程可以继续运行。

如果对象调用了notifyAll方法就会通知所有等待这个对象控制权的线程继续运行。
```



**测试：**

​		出现交替+1  -1



**需求变更：**

​		要求使用四个多线程（两个加，两个减）来模拟对数值的操作

**方案：**

​		直接增加多线程为4个

```java
        new Thread(()->{......},"A").start();
        new Thread(()->{......},"B").start();
        new Thread(()->{......},"C").start();
        new Thread(()->{......},"D").start();
```

**测试出现的问题：**

​		在运行过程中会`出现 0,1,2,3 等多种情况`

**原因：**

​		多线程交互中，会出现虚假唤醒现象

**改进：**

​		将判断条件 if 修改为 while

**原理：**

​		防止虚假唤醒(判断只能用while，不能用if)

**改进后代码：**

```java
/**
 * ThreadWaitNotifyDemo     synchronized版
 * 题目：现在两个线程，可以操作初始值为零的一个变量，
 * 实现一个线程对该变量加1，一个线程对该变量-1，
 * 实现交替，来10轮，变量初始值为0.
 * 1. 高内聚低耦合前提下，线程操作资源类
 * 2. 交互中，判断/干活/通知
 * 3. 多线程交互中，防止虚假唤醒(判断只能用while，不能用if)
 * 知识小总结：多线程编程套路+while判断+新版写法
 * @author daixl
 * @date 2021/6/14
 */
public class ThreadWaitNotifyDemo1 {

    public static void main(String[] args) {
        AirConditioner1 airConditioner = new AirConditioner1();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    airConditioner.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "A").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    airConditioner.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "B").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    airConditioner.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "C").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    airConditioner.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "D").start();
    }
}

/**
 * 资源类
 */
class AirConditioner1 {

    private int number = 0;

    public synchronized void increment() throws InterruptedException {
        //1.判断，不能用if，只能用while，防止虚拟唤醒
//        if (number != 0){
//            this.wait();
//        }
        while (number != 0){
            this.wait();
        }
        //2.干活
        number++;
        System.out.println(Thread.currentThread().getName() + "\t" + number);
        //3.通知
        this.notifyAll();
    }

    public synchronized void decrement() throws InterruptedException {
        //1.判断
//        if (number == 0){
//            this.wait();
//        }
        while (number == 0) {
            this.wait();
        }
        //2.干活
        number--;
        System.out.println(Thread.currentThread().getName() + "\t" + number);
        //3.通知
        this.notifyAll();
    }
}
```



## 04. java8新版实现

**使用 lock 替换 synchronized**

![image-20210421195008096](https://img-blog.csdnimg.cn/img_convert/fc0fcc3c4e01ed3e7cc4eaabfea6d367.png)

```java
/**
 * ThreadWaitNotifyDemo         lock版
 * 题目：现在两个线程，可以操作初始值为零的一个变量，
 * 实现一个线程对该变量加1，一个线程对该变量-1，
 * 实现交替，来10轮，变量初始值为0.
 * 1. 高内聚低耦合前提下，线程操作资源类
 * 2. 交互中，判断/干活/通知
 * 3. 多线程交互中，防止虚假唤醒(判断只能用while，不能用if)
 * 知识小总结：多线程编程套路+while判断+新版写法
 * @author daixl
 * @date 2021/6/14
 */
public class ThreadWaitNotifyDemo2 {

    public static void main(String[] args) {
        AirConditioner2 airConditioner = new AirConditioner2();
        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                airConditioner.increment();
            }
        }, "A").start();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                airConditioner.decrement();
            }
        }, "B").start();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                airConditioner.increment();
            }
        }, "C").start();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                airConditioner.decrement();
            }
        }, "D").start();
    }
}

/**
 * 资源类
 */
class AirConditioner2 {
    private int number = 0;
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    public void increment() {
        lock.lock();
        try {
            while (number != 0) {
                condition.await();//this.wait();
            }
            number++;
            System.out.println(Thread.currentThread().getName() + "\t" + number);
            condition.signalAll();//this.notify();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void decrement() {
        try {
            lock.lock();
            while (number == 0) {
                condition.await();//this.wait();
            }
            number--;
            System.out.println(Thread.currentThread().getName() + "\t" + number);
            condition.signalAll();//this.notify();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
```

**补充：** [生产者消费者模式的实现(java实现)](https://blog.csdn.net/shadowcw/article/details/82352829)

```apl
在Java中一共有四种方法支持同步，其中前三个是同步方法，一个是管道方法。

1. 使用Object的wait() / notify()方法
2. 使用Lock和Condition的await() / signal()方法
3. 使用BlockingQueue阻塞队列方法
4. PipedInputStream / PipedOutputStream
    
本文只介绍最常用的前三种，第四种暂不做讨论，
```



# 五、线程间定制化调用通信

**题目描述：**

多线程之间按顺序调用，实现A->B->C
三个线程启动，要求如下：
AA打印5次，BB打印10次，CC打印15次
接着
AA打印5次，BB打印10次，CC打印15次
来10轮

**代码：**

```java
package com.atguigu.sh.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ThreadOrderAccess
 * 题目：多线程之间按顺序调用，实现A->B->C
 * 三个线程启动，要求如下：
 * AA打印5次，BB打印10次，CC打印15次
 * 接着
 * AA打印5次，BB打印10次，CC打印15次
 * 来10轮
 *      1.高内聚低耦合前提下，线程操作资源类
 *      2.判断/干活/通知
 *      3.多线程交互中，防止虚假唤醒(判断只能用while，不能用if)
 *      4.标志位
 * @author daixl
 * @date 2021/6/15
 */
public class ThreadOrderAccess {

    public static void main(String[] args) {
        ShareResource shareResource = new ShareResource();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                shareResource.print5();
            }
        }, "A").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                shareResource.print10();
            }
        }, "B").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                shareResource.print15();
            }
        }, "C").start();
    }
}

/**
 * 资源类
 */
class ShareResource{

    private int number = 1; //1:A   2:B    3:C
    private Lock lock = new ReentrantLock();
    private Condition condition1 = lock.newCondition();
    private Condition condition2 = lock.newCondition();
    private Condition condition3 = lock.newCondition();

    public void print5(){
        lock.lock();
        try {
            //1.判断
            while (number != 1) {
                condition1.await();
            }
            //2.干活
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + "\t" + "A");
            }
            //3.通知
            number = 2;
            condition2.signal();//唤醒condition2
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("失败啦");
        } finally {
            lock.unlock();
        }
    }

    public void print10(){
        lock.lock();
        try {
            //1.判断
            while (number != 2){
                condition2.await();
            }
            //2.干活
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + "\t" + "B");
            }
            //3.通知
            number = 3;
            condition3.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
    public void print15(){
        lock.lock();
        try {
            //1.判断
            while (number != 3){
                condition3.await();
            }
            //2.干活
            for (int i = 0; i < 15; i++) {
                System.out.println(Thread.currentThread().getName() + "\t" + "C");
            }
            //3.通知
            number = 1;
            condition1.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
```

**描述：**

​		使用`Lock`结合`Condition`的`await()`、`singnal`精准唤醒某个线程。

**总结：**

```
/**
 * 题目：多线程之间按顺序调用，实现A->B->C
 * 三个线程启动，要求如下：
 * AA打印5次，BB打印10次，CC打印15次
 * 接着
 * AA打印5次，BB打印10次，CC打印15次
 * 来10轮
 *
 *
 *      1.高内聚低耦合前提下，线程操作资源类
 *      2.判断/干活/通知
 *      3.多线程交互中，防止虚假唤醒(判断只能用while，不能用if)
 *      4.标志位
 */
```



# 六、多线程锁

## 01. 锁的8个问题

1 标准访问，先打印短信还是邮件
2 停4秒在短信方法内，先打印短信还是邮件
3 普通的hello方法，是先打短信还是hello
4 现在有两部手机，先打印短信还是邮件
5 两个静态同步方法，1部手机，先打印短信还是邮件
6 两个静态同步方法，2部手机，先打印短信还是邮件
7 1个静态同步方法，1个普通同步方法，1部手机，先打印短信还是邮件
8 1个静态同步方法，1个普通同步方法，2部手机，先打印短信还是邮件

 

```apl
运行答案：
1、短信
2、短信
3、Hello
4、邮件
5、短信
6、短信
7、邮件
8、邮件
```



## 02. 8锁分析

```java
/**
 * 题目，多线程8锁
 * 1.标准访问，先打印邮件
 * 2.邮件设置暂停4秒方法，先打印邮件
 *      对象锁
 *      一个对象里面如果有多个synchronized方法，某一个时刻内，只要一个线程去调用其中的一个synchronized方法了，
 *      其他的线程都只能等待，换句话说，某一个时刻内，只能有唯一一个线程去访问这些synchronized方法，
 *      锁的是当前对象this，被锁定后，其他的线程都不能进入到当前对象的其他的synchronized方法
 * 3.新增sayHello方法，先打印sayHello
 *      加个普通方法后发现和同步锁无关
 * 4.两部手机，先打印短信
 *      换成两个对象后，不是同一把锁了，情况立刻变化
 * 5.两个静态同步方法，同一部手机，先打印邮件
 * 6.两个静态同步方法，同两部手机，先打印邮件，锁的同一个字节码对象
 *      全局锁
 *      synchronized实现同步的基础：java中的每一个对象都可以作为锁。
 *      具体表现为一下3中形式。
 *      对于普通同步方法，锁是当前实例对象，锁的是当前对象this，
 *      对于同步方法块，锁的是synchronized括号里配置的对象。
 *      对于静态同步方法，锁是当前类的class对象
 * 7.一个静态同步方法，一个普通同步方法，同一部手机，先打印短信
 * 8.一个静态同步方法，一个普通同步方法，同二部手机，先打印短信
 *      当一个线程试图访问同步代码块时，它首先必须得到锁，退出或抛出异常时必须释放锁。
 *      也就是说如果一个实例对象的普通同步方法获取锁后，该实例对象的其他普通同步方法必须等待获取锁的方法释放锁后才能获取锁，
 *      可是别的实例对象的普通同步方法因为跟该实例对象的普通同步方法用的是不同的锁，
 *      所以无需等待该实例对象已获取锁的普通同步方法释放锁就可以获取他们自己的锁。
 *
 *      所有的静态同步方法用的也是同一把锁--类对象本身，
 *      这两把锁(this/class)是两个不同的对象，所以静态同步方法与非静态同步方法之间是不会有静态条件的。
 *      但是一旦一个静态同步方法获取锁后，其他的静态同步方法都必须等待该方法释放锁后才能获取锁，
 *      而不管是同一个实例对象的静态同步方法之间，
 *      还是不同的实例对象的静态同步方法之间，只要它们同一个类的实例对象
 *
 * @author daixl
 * @date 2021/6/16
 */
```

## 03. 代码

```java
package com.atguigu.sh.juc;

import java.util.concurrent.TimeUnit;

/**
 * 题目，多线程8锁
 * 1.标准访问，先打印邮件
 * 2.邮件设置暂停4秒方法，先打印邮件
 *      对象锁
 *      一个对象里面如果有多个synchronized方法，某一个时刻内，只要一个线程去调用其中的一个synchronized方法了，
 *      其他的线程都只能等待，换句话说，某一个时刻内，只能有唯一一个线程去访问这些synchronized方法，
 *      锁的是当前对象this，被锁定后，其他的线程都不能进入到当前对象的其他的synchronized方法
 * 3.新增sayHello方法，先打印sayHello
 *      加个普通方法后发现和同步锁无关
 * 4.两部手机，先打印短信
 *      换成两个对象后，不是同一把锁了，情况立刻变化
 * 5.两个静态同步方法，同一部手机，先打印邮件
 * 6.两个静态同步方法，同两部手机，先打印邮件，锁的同一个字节码对象
 *      全局锁
 *      synchronized实现同步的基础：java中的每一个对象都可以作为锁。
 *      具体表现为一下3中形式。
 *      对于普通同步方法，锁是当前实例对象，锁的是当前对象this，
 *      对于同步方法块，锁的是synchronized括号里配置的对象。
 *      对于静态同步方法，锁是当前类的class对象
 * 7.一个静态同步方法，一个普通同步方法，同一部手机，先打印短信
 * 8.一个静态同步方法，一个普通同步方法，同二部手机，先打印短信
 *      当一个线程试图访问同步代码块时，它首先必须得到锁，退出或抛出异常时必须释放锁。
 *      也就是说如果一个实例对象的普通同步方法获取锁后，该实例对象的其他普通同步方法必须等待获取锁的方法释放锁后才能获取锁，
 *      可是别的实例对象的普通同步方法因为跟该实例对象的普通同步方法用的是不同的锁，
 *      所以无需等待该实例对象已获取锁的普通同步方法释放锁就可以获取他们自己的锁。
 *
 *      所有的静态同步方法用的也是同一把锁--类对象本身，
 *      这两把锁(this/class)是两个不同的对象，所以静态同步方法与非静态同步方法之间是不会有静态条件的。
 *      但是一旦一个静态同步方法获取锁后，其他的静态同步方法都必须等待该方法释放锁后才能获取锁，
 *      而不管是同一个实例对象的静态同步方法之间，
 *      还是不同的实例对象的静态同步方法之间，只要它们同一个类的实例对象
 *
 * @author daixl
 * @date 2021/6/16
 */
public class Lock8 {

    public static void main(String[] args) {
        Phone phone = new Phone();
        Phone phone2 = new Phone();

        new Thread(() -> {
            try {
                phone.sendEmail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "A").start();
        
        new Thread(() -> {
            try {
               phone.sendSMS();
//                phone.hello();
//                phone2.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "B").start();
    }
}

/**
 * 资源类
 */
class Phone{

    public static synchronized void sendEmail() throws Exception{
        //暂停一会儿线程
        try { TimeUnit.SECONDS.sleep( 4 ); } catch (InterruptedException e) { e.printStackTrace(); }
        System.out.println("sendEmail");
    }

    public static synchronized void sendSMS() throws Exception{
        System.out.println("sendSMS");
    }

    public void hello() throws Exception{
        System.out.println("hello");
    }
}
```



 ## 04. 总结

```apl
1 一个对象里面如果有多个synchronized方法，某一个时刻内，只要一个线程去调用其中的一个synchronized方法了，其他的线程都只能等待，换句话说，某一时刻内，只能有唯一一个线程去访问这些synchronized方法。----因为锁的是当前对象this，被锁定后，其他线程都不能进入到当前对象的其他的synchronized方法。

2 加个普通方法后发现和同步锁无关。---因为普通方法和同步锁无关

3 换成静态同步方法后，情况又变化.----因为静态同步方法用的锁的是class对象

4 所有的非静态同步方法用的都是同一把锁 -- 实例对象本身，也就是说如果一个实例对象的非静态同步方法获取锁后，该实例对象的其他非静态同步方法必须等待获取锁的方法释放锁后才能获取锁，可是别的实例对象的非静态同步方法因为跟该实例对象的非静态同步方法用的是不同的锁，所以毋须等待该实例对象已经取锁的非静态同步方法释放锁就可以获取他们自己的锁。

⑥ 所有的静态同步方法用的也是同一把锁 -- 类对象本身，这两把锁是两个不同的对象，所以静态同步方法与非静态同步方法之间不会有竞争条件。但是一旦一个静态同步方法获取锁后，其他的静态同步方法都必须等待该方法释放锁后才能获取锁，而不管是同一个实例对象的静态同步方法之间，还是不同的实例对象的静态同步方法之间，只要它们是同一个实例对象
```

**补充解释static关键字：**

https://blog.csdn.net/kuangay/article/details/81485324



# 七、集合不安全类

## 01. List

**java.util.ConcurrentModificationException 并发修改异常**

```java
package lut.juc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class demo06 {
    public static void main(String[] args) {
        List<String> list=new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            },String.valueOf(i)).start();
        }
    }
}


/***
ArrayList在迭代的时候如果同时对其进行修改就会抛出java.util.ConcurrentModificationException异常   并发修改异常

当 i<3 使，系统正常运行，但每次结果不一样
***/
```

**原因：**

```java
看ArrayList的源码
public boolean add(E e) {
    ensureCapacityInternal(size + 1);  // Increments modCount!!
    elementData[size++] = e;
    return true;
}
没有synchronized线程不安全
```



**解决方案**

* Vector
* Collections
* CopyOnWriteArrayList（写时复制）



**1.new Vector<>();**

```java
List<String> list = new Vector<>();
```



![image-20210617002407721](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210617002407721.png)

看Vector的源码:

```java
public synchronized boolean add(E e) {
    modCount++;
    ensureCapacityHelper(elementCount + 1);
    elementData[elementCount++] = e;
    return true;
}
//有synchronized线程安全
```



```java
public class demo06 {
    public static void main(String[] args) {
        List<String> list=new Vector<>();
        for (int i = 0; i < 30; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            },String.valueOf(i)).start();
        }
    }
}

/***
保证了数据一致性，加锁没有保证并发性
**/
```

**2.Collections.synchronizedList(new ArrayList());**

```java
List<String> list = Collections.synchronizedList(new ArrayList<>());
```

`Collections`提供了方法`synchronizedList`保证`list`是同步线程安全的

那`HashMap`，`HashSet`是线程安全的吗？也不是
所以有同样的线程安全方法

![image-20210617002737205](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210617002737205.png)



```java
public class demo06 {
    public static void main(String[] args) {
        List<String> list= Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < 30; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            },String.valueOf(i)).start();
        }
    }
}
```

**3.new CopyOnWriteArrayList(); //写时复制**

​		不加锁性能提升出错误，加锁数据一致性能下降

CopyOnWriteArrayList定义：

```java
A thread-safe variant of ArrayList in which all mutative operations (add, set, and so on) are implemented by making a fresh copy of the underlying array.
CopyOnWriteArrayList是arraylist的一种线程安全变体，
其中所有可变操作（add、set等）都是通过生成底层数组的新副本来实现的。
```

举例：名单签到：

![image-20210617002909629](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210617002909629.png)

CopyOnWrite理论：

```java
 
/**
 * Appends the specified element to the end of this list.
 *
 * @param e element to be appended to this list
 * @return {@code true} (as specified by {@link Collection#add})
 */
public boolean add(E e) {
    final ReentrantLock lock = this.lock;
    lock.lock();
    try {
        Object[] elements = getArray();
        int len = elements.length;
        Object[] newElements = Arrays.copyOf(elements, len + 1);
        newElements[len] = e;
        setArray(newElements);
        return true;
    } finally {
        lock.unlock();
    }
}

/**
CopyOnWrite容器即写时复制的容器。往一个容器添加元素的时候，不直接往当前容器Object[]添加，
而是先将当前容器Object[]进行Copy，复制出一个新的容器Object[] newElements，然后向新的容器Object[] newElements里添加元素。
添加元素后，再将原容器的引用指向新的容器setArray(newElements)。
这样做的好处是可以对CopyOnWrite容器进行并发的读，而不需要加锁，因为当前容器不会添加任何元素。
所以CopyOnWrite容器也是一种读写分离的思想，读和写不同的容器。
*/
```

代码：

```java
package com.atguigu.sh.juc;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 题目：请举例说明集合类是不安全的
 * 1    故障现象
 *      java.util.ConcurrentModificationException
 * 2    导致原因
 *      线程不安全
 * 3    解决方法
 *   3.1 Vector
 *   3.2 Collections.synchronizedList(new ArrayList<>())
 *   3.3 new CopyOnWriteArrayList<>()
 *
 * @author daixl
 * @date 2021/6/16
 */
public class NotSafeDemo {

    public static void main(String[] args) {
        List<String> list = new CopyOnWriteArrayList<>();//Collections.synchronizedList(new ArrayList<>());//new Vector<>();//new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                list.add(UUID.randomUUID().toString().substring(0, 8));
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }
}
```



## 02. HashSet

线程不安全

**解决方案：**

* `Collections.synchronizedSet(new HashSet<String>())`
* `new CopyOnWriteArraySet<>()`

```java
/**
 * 题目：请举例说明集合类是不安全的
 * 1    故障现象
 *      java.util.ConcurrentModificationException
 * 2    导致原因
 *      线程不安全
 * 3    解决方法
 *   3.1 Collections.synchronizedSet(new HashSet<String>())
 *   3.2 new CopyOnWriteArraySet<>()
 *
 * @author daixl
 * @date 2021/6/17
 */
public class NotSafeDemo2 {

    public static void main(String[] args) {
        Set<String> set = new CopyOnWriteArraySet<>();//Collections.synchronizedSet(new HashSet<String>());//new HashSet<String>();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                set.add(UUID.randomUUID().toString().substring(0, 8));
                System.out.println(set);
            }, String.valueOf(i)).start();
        }
    }
}
```



**问题：**

```java
Set<String> set = new HashSet<>();//线程不安全
Set<String> set = new CopyOnWriteArraySet<>();//线程安全
```



HashSet底层数据结构是什么？
HashMap  ?

但HashSet的add是放一个值，而HashMap是放K、V键值对

 **原因：**

* 底层`HashMap`
* 值存在key中，value放的是常量，由于hashmap的key不重复，所以set不重复

 **源码：**

```java
public HashSet() {
    map = new HashMap<>();
}

private static final Object PRESENT = new Object();

public boolean add(E e) {
    return map.put(e, PRESENT)==null;
}
```



## 03. HashMap

线程不安全

**解决方案：**

* `Collections.synchronizedMap(new HashMap<>())`
* `new ConcurrentHashMap<>()`

```java
/**
 * 题目：请举例说明集合类是不安全的
 * 1    故障现象
 *      java.util.ConcurrentModificationException
 * 2    导致原因
 *      线程不安全
 * 3    解决方法
 *   3.1 Collections.synchronizedMap(new HashMap<>())
 *   3.2 new ConcurrentHashMap<>()
 *
 * @author daixl
 * @date 2021/6/17
 */
public class NotSafeDemo3 {

    public static void main(String[] args) {
        Map<String, String> map = new ConcurrentHashMap<>();//Collections.synchronizedMap(new HashMap<>());//new HashMap<>();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0, 8));
                System.out.println(map);
            }, String.valueOf(i)).start();
        }
    }
}
```



 问题：

* 默认长度16，factor0.75
* **HashMap由数组+链表组成的**

<img src="C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210617222841871.png" alt="image-20210617222841871" style="zoom:50%;" />

​		哈希表的每个位置，存的是一个`Node<key, value>`单向链表

**源码：**

```java
    public V put(K key, V value) {
        return putVal(hash(key), key, value, false, true);
    }

    final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                   boolean evict) {
        Node<K,V>[] tab; Node<K,V> p; int n, i;
        if ((tab = table) == null || (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((p = tab[i = (n - 1) & hash]) == null)
            tab[i] = newNode(hash, key, value, null);
        else {
            Node<K,V> e; K k;
            if (p.hash == hash &&
                ((k = p.key) == key || (key != null && key.equals(k))))
                e = p;
            else if (p instanceof TreeNode)
                e = ((TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
            else {
                for (int binCount = 0; ; ++binCount) {
                    if ((e = p.next) == null) {
                        p.next = newNode(hash, key, value, null);
                        if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                            treeifyBin(tab, hash);
                        break;
                    }
                    if (e.hash == hash &&
                        ((k = e.key) == key || (key != null && key.equals(k))))
                        break;
                    p = e;
                }
            }
            if (e != null) { // existing mapping for key
                V oldValue = e.value;
                if (!onlyIfAbsent || oldValue == null)
                    e.value = value;
                afterNodeAccess(e);
                return oldValue;
            }
        }
        ++modCount;
        if (++size > threshold)
            resize();
        afterNodeInsertion(evict);
        return null;
    }
```

补充：

HashMap原理：https://blog.csdn.net/woshimaxiao1/article/details/83661464



# 八、Callable接口

## 01. 是什么

面试题：获得多线程的方法几种？

（1）继承thread类（2）runnable接口
如果只回答这两个你连被问到juc的机会都没有 

**正确答案如下：**
 		传统的是继承thread类和实现runnable接口，
		 java5以后又有实现callable接口和java的线程池获得

**扩展**：[JAVA多线程实现的三种方式](https://blog.csdn.net/aboy123/article/details/38307539)

**函数式接口：**

 ![image-20210617235328640](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210617235328640.png)

## 02. 与runnable对比

```java
//创建新类MyThread实现runnable接口
class MyThread implements Runnable{
 @Override
 public void run() {
 
 }
}
//新类MyThread2实现callable接口
class MyThread2 implements Callable<Integer>{
 @Override
 public Integer call() throws Exception {
  return 200;
 } 
}
```

 面试题: callable接口与runnable接口的区别？

 答：（1）是否有返回值
        （2）是否抛异常
        （3）落地方法不一样，一个是run，一个是call

## 03. 怎么用

**直接替换runnable是否可行？**

不可行，因为：thread类的构造方法根本没有Callable

![image-20210617235549344](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210617235549344.png)



**认识不同的人找中间人:**

这像认识一个不认识的同学，我可以找中间人介绍。
---中间人是什么？java多态，一个类可以实现多个接口！！

![image-20210617235637751](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210617235637751.png)

```java
FutureTask<Integer> ft = new FutureTask<Integer>(new MyThread());
new Thread(ft, "AA").start();
```

运行成功后如何获得返回值？

![image-20210617235718232](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210617235718232.png)

```java
ft.get();
```



## 04. FutureTask

**是什么:**

未来的任务，用它就干一件事，==**异步调用**==
main方法就像一个冰糖葫芦，一个个方法由main串起来。
但解决不了一个问题：正常调用挂起堵塞问题

![image-20210618000332548](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210618000332548.png)

例子：
（1）老师上着课，口渴了，去买水不合适，讲课线程继续，我可以单起个线程找班长帮忙买水，水买回来了放桌上，我需要的时候再去get。
（2）4个同学，A算1+20,B算21+30,C算31*到40,D算41+50，是不是C的计算量有点大啊，FutureTask单起个线程给C计算，我先汇总ABD，最后等C计算完了再汇总C，拿到最终结果
（3）高考：会做的先做，不会的放在后面做



**原理:**


​		在主线程中需要执行比较耗时的操作时，但又不想阻塞主线程时，可以把这些作业交给Future对象在后台完成，当主线程将来需要时，就可以通过Future对象获得后台作业的计算结果或者执行状态。

​		一般FutureTask多用于耗时的计算，主线程可以在完成自己的任务后，再去获取结果。

​		仅在计算完成时才能检索结果；如果计算尚未完成，则阻塞 get 方法。一旦计算完成，就不能再重新开始或取消计算。get方法而获取结果只有在计算完成时获取，否则会一直阻塞直到任务转入完成状态，然后会返回结果或者抛出异常。 

​		==只计算一次==
​		==get方法放到最后==

**继承方式：**

![image-20210426091927187](https://img-blog.csdnimg.cn/img_convert/39e3b2f0669ac7934e139583cc9778b5.png)

**代码：**

```java
class MyThread implements Runnable{

    @Override
    public void run() {

    }
}
class MyThread2 implements Callable<Integer>{

    @Override
    public Integer call() throws Exception {
        System.out.println(Thread.currentThread().getName()+"come in callable");
        return 200;
    }
}


public class CallableDemo {


    public static void main(String[] args) throws Exception {

        //FutureTask<Integer> futureTask = new FutureTask(new MyThread2());
        FutureTask<Integer> futureTask = new FutureTask(()->{
            System.out.println(Thread.currentThread().getName()+"  come in callable");
            TimeUnit.SECONDS.sleep(4);
            return 1024;
        });
        FutureTask<Integer> futureTask2 = new FutureTask(()->{
            System.out.println(Thread.currentThread().getName()+"  come in callable");
            TimeUnit.SECONDS.sleep(4);
            return 2048;
        });

        new Thread(futureTask,"zhang3").start();
        new Thread(futureTask2,"li4").start();

        //System.out.println(futureTask.get());
        //System.out.println(futureTask2.get());
        //1、一般放在程序后面，直接获取结果
        //2、只会计算结果一次

        while(!futureTask.isDone()){
            System.out.println("***wait");
        }
        System.out.println(futureTask.get());
        System.out.println(Thread.currentThread().getName()+" come over");
    }
}
 
/**
 * 
 * 
在主线程中需要执行比较耗时的操作时，但又不想阻塞主线程时，可以把这些作业交给Future对象在后台完成，
当主线程将来需要时，就可以通过Future对象获得后台作业的计算结果或者执行状态。
 
一般FutureTask多用于耗时的计算，主线程可以在完成自己的任务后，再去获取结果。
 
仅在计算完成时才能检索结果；如果计算尚未完成，则阻塞 get 方法。一旦计算完成，
就不能再重新开始或取消计算。get方法而获取结果只有在计算完成时获取，否则会一直阻塞直到任务转入完成状态，
然后会返回结果或者抛出异常。 
 
只计算一次
get方法放到最后
 */
```



# 九、JUC强大的辅助类讲解

## 01. CountDownLatch减少计数--离开关门
常理来说：其他线程结束之后，主线程才结束

存在的问题演示：

```java
public class demo08 {
    public static void main(String[] args) {
        for (int i = 0; i <6 ; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"~离开教室");
            },String.valueOf(i)).start();
        }
        System.out.println("班长关门");
    }
}

/**
0~离开教室
2~离开教室
3~离开教室
5~离开教室
班长关门
1~离开教室
4~离开教室
**/
```

**使用CountDownLatch**

```java
/**
 * CountDownLatch主要有两个方法，当一个或多个线程调用await方法时，这些线程会阻塞。
 * 其它线程调用countDown方法会将计数器减1(调用countDown方法的线程不会阻塞)，
 * 当计数器的值变为0时，因await方法阻塞的线程会被唤醒，继续执行。
 *
 * @author daixl
 * @date 2021/6/19
 */
public class CountDownLatchDemo {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(6);//减少计数
        for (int i = 0; i < 6; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "\t离开教室");
                countDownLatch.countDown();
            }, String.valueOf(i)).start();
        }
        countDownLatch.await();//countDownLatch计数为0时，主线程才会被唤醒
        System.out.println(Thread.currentThread().getName() + "\t班长关门走人");
    }

    public static void closeDoor() {
        for (int i = 0; i < 6; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "\t离开教室");
            }, String.valueOf(i)).start();
        }
        System.out.println(Thread.currentThread().getName() + "\t班长关门走人");
    }
}

/**
 0~离开教室
 2~离开教室
 1~离开教室
 3~离开教室
 4~离开教室
 5~离开教室
 班长关门
 */
```



**说明：**

```apl
/*
 * CountDownLatch主要有两个方法，当一个或多个线程调用await方法时，这些线程会阻塞。
 * 其他线程调用countDown方法会将计数器减1(调用countDown方法的线程不会阻塞)，
 * 当计数器的值变为0时，因await方法阻塞的线程会被唤醒，继续执行
 */
```



## 02. CyclicBarrier循环栅栏---等待开会

集齐七颗龙珠召唤神龙



```java
/**
 * CyclicBarrier
 * 的字面意思是可循环（Cyclic）使用的屏障（Barrier）。它要做的事情是，
 * 让一组线程到达一个屏障（也可以叫同步点）时被阻塞，
 * 直到最后一个线程到达屏障时，屏障才会开门，所有
 * 被屏障拦截的线程才会继续干活。
 * 线程进入屏障通过CyclicBarrier的await()方法。
 *
 * @author daixl
 * @date 2021/6/19
 */
public class CyclicBarrierDemo {

    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7, () -> {
            System.out.println("召唤神龙");
        });
        for (int i = 0; i < 7; i++) {
            final int tempInt = i;
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "\t集齐第：" + tempInt + "颗龙珠");
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }, String.valueOf(i)).start();
        }
    }
}

/*
0找到第0颗龙珠
1找到第1颗龙珠
2找到第2颗龙珠
3找到第3颗龙珠
6找到第6颗龙珠
4找到第4颗龙珠
5找到第5颗龙珠
**召唤神龙**
 */
```



**说明：**

```apl
 * CyclicBarrier
 * 的字面意思是可循环（Cyclic）使用的屏障（Barrier）。它要做的事情是，
 * 让一组线程到达一个屏障（也可以叫同步点）时被阻塞，
 * 直到最后一个线程到达屏障时，屏障才会开门，所有
 * 被屏障拦截的线程才会继续干活。
 * 线程进入屏障通过CyclicBarrier的await()方法。
```



## 03. Semaphore信号灯--抢车位
主要用于资源的并发控制

当 new Semaphore(1) 时相当于 synchronized

Demo：synchronized

```java
package com.atguigu.sh.juc;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * 控制线程的并发数，抢车位
 * 在信号量上我们定义两种操作：
 * acquire（获取） 当一个线程调用acquire操作时，它要么通过成功获取信号量（信号量减1），
 *       要么一直等下去，直到有线程释放信号量，或超时。
 * release（释放）实际上会将信号量的值加1，然后唤醒等待的线程。
 *
 * 信号量主要用于两个目的，一个是用于多个共享资源的互斥使用，另一个用于并发线程数的控制。
 * @author daixl
 * @date 2021/6/19
 */
public class SemaphoreDemo {

    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);//三个停车位
        for (int i = 0; i < 6; i++) {
            new Thread(() -> {
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + "\t抢到车位了");
                    //暂停一会儿线程
                    try { TimeUnit.SECONDS.sleep( 3 ); } catch (InterruptedException e) { e.printStackTrace(); }
                    System.out.println(Thread.currentThread().getName() + "\t离开车位了");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                }
            }, String.valueOf(i)).start();
        }
    }
}
/**
1抢到了车位
2抢到了车位
0抢到了车位
0离开了车位
1离开了车位
2离开了车位
3抢到了车位
5抢到了车位
4抢到了车位
3离开了车位
4离开了车位
6抢到了车位
5离开了车位
6离开了车位

**/
```



**说明：**

```apl
在信号量上我们定义两种操作:
* acquire(获取）当一个线程调用acquire操作时，它要么通过成功获取信号量（信号量减1),要么一直等下去，直到有线程释放信号量，或超时。
* release（释放）实际上会将信号量的值加1，然后唤醒等待的线程。
*信号量主要用于两个目的，一个是用于多个共享资源的互斥使用，另一个用于并发线程数的控制心
```



# 十、ReentrantReadWriteLock读写锁
```java
/**
 * 多个线程同时读一个资源类没有任何问题，所以为了满足并发量，读取共享资源应该可以同时进行。
 * 但是，如果有一个线程想去写共享资源来，就不应该再有其他线程可以对改资源进行读或写
 * 小总结：
 *      读-读能共存
 *      读-写不能共存
 *      写-写不能共存
 */
```

**存在的问题：当有一个写入操作时，又有其他的操作插进来，破坏了事务的原子性，具体如下：**

```java
class MyCache {
    private volatile Map<String, Object> map = new HashMap<>();

    public void put(String key, Object value) {
        System.out.println(Thread.currentThread().getName() + "写入数据key" + key);
        map.put(key, value);
        System.out.println(Thread.currentThread().getName() + "写入完成key" + key);
    }

    public void get(String key) {
        System.out.println(Thread.currentThread().getName() + "读取数据key" + key);
        map.get(key);
        System.out.println(Thread.currentThread().getName() + "读取完成key" + key);
    }


}

public class demo11 {
    public static void main(String[] args) {
        MyCache myCache = new MyCache();
        for (int i = 1; i <= 5; i++) {
            final int tempInt = i;
            new Thread(() -> {
                myCache.put(tempInt + "", tempInt + "");
            }, String.valueOf(i)).start();
        }
        for (int i = 1; i <= 5; i++) {
            final int tempInt = i;
            new Thread(() -> {
                myCache.get(tempInt + "");
            }, String.valueOf(i)).start();
        }
    }
}


/**
  2写入数据key2
 3写入数据key3
 3写入完成key3
 5写入数据key5
 5写入完成key5
 1写入数据key1
 4写入数据key4
 1写入完成key1
 2写入完成key2
 4写入完成key4
 1读取数据key1
 1读取完成key1
 3读取数据key3
 3读取完成key3
 2读取数据key2
 2读取完成key2
 4读取数据key4
 4读取完成key4
 5读取数据key5
 5读取完成key5
 **/
```



**加入读写锁**

```java

class MyCache {
    private volatile Map<String, Object> map = new HashMap<>();
    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public void put(String key, Object value) {
        readWriteLock.writeLock().lock();//写锁，不允许其他读、写操作
        try {
            System.out.println(Thread.currentThread().getName() + "写入数据key" + key);
            map.put(key, value);
            System.out.println(Thread.currentThread().getName() + "写入完成key" + key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }


    public void get(String key) {
        readWriteLock.writeLock().lock();//读锁
        try {
            System.out.println(Thread.currentThread().getName() + "读取数据key" + key);
            map.get(key);
            System.out.println(Thread.currentThread().getName() + "读取完成key" + key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }


}

public class demo11 {
    public static void main(String[] args) {
        MyCache myCache = new MyCache();
        for (int i = 1; i <= 5; i++) {
            final int tempInt = i;
            new Thread(() -> {
                myCache.put(tempInt + "", tempInt + "");
            }, String.valueOf(i)).start();
        }
        for (int i = 1; i <= 5; i++) {
            final int tempInt = i;
            new Thread(() -> {
                myCache.get(tempInt + "");
            }, String.valueOf(i)).start();
        }
    }
}

/**
 2写入数据key2
 2写入完成key2
 5写入数据key5
 5写入完成key5
 3写入数据key3
 3写入完成key3
 1写入数据key1
 1写入完成key1
 4写入数据key4
 4写入完成key4
 1读取数据key1
 1读取完成key1
 2读取数据key2
 2读取完成key2
 3读取数据key3
 3读取完成key3
 4读取数据key4
 4读取完成key4
 5读取数据key5
 5读取完成key5
 **/
```



# 十一、BlockingQueueDemo阻塞队列

**阻塞队列：**

![image-20210426212053395](https://img-blog.csdnimg.cn/img_convert/686b98288126b9eebb5cf4885a94f63d.png)

```apl
当队列是空的，从队列中获取元素的操作将会被阻塞

当队列是满的，从队列中添加元素的操作将会被阻塞

试图从空的队列中获取元素的线程将会被阻塞，直到其他线程往空的队列插入新的元幸

试图向已满的队列中添加新元素的线程将会被阻塞，直到其他线程从队列中移除一个或多个元素或者完全清空，使队列变得空闲起来并后续新增
```

**栈与队列：**

* 栈：先进后出，后进先出
* 队列：先进先出



**阻塞队列的用处：**

​		在多线程领域：所谓阻塞，在某些情况下会挂起线程（即阻塞），一旦条件满足，被挂起的线程又会自动被唤起

为什么需要BlockingQueue

* 好处是我们不需要关心什么时候需要阻塞线程，什么时候需要唤醒线程，因为这一切BlockingQueue都给你一手包办了

* 在concurrent包发布以前，在多线程环境下，我们每个程序员都必须去自己控制这些细节，尤其还要兼顾效率和线程安全，而这会给我们的程序带来不小的复杂度。
* *某方面可以替换await()、notify()



**架构介绍：**

![image-20210426213012495](https://img-blog.csdnimg.cn/img_convert/6724ea7223e299d970b3a52619b03953.png)



**种类分析：**

* ==ArrayBlockingQueue：由数组结构组成的有界阻塞队列。==
* ==LinkedBlockingQueue：由链表结构组成的有界（但大小默认值为integer.MAX_VALUE）阻塞队列。==
* PriorityBlockingQueue：支持优先级排序的无界阻塞队列。
* DelayQueue：使用优先级队列实现的延迟无界阻塞队列。
* ==SynchronousQueue：不存储元素的阻塞队列，也即单个元素的队列。==
* LinkedTransferQueue：由链表组成的无界阻塞队列。
* LinkedBlockingDeque：由链表组成的双向阻塞队列。



**BlockingQueue核心方法：**

![image-20210620015354856](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210620015354856.png)



| 抛出异常 | 当阻塞队列满时，再往队列里add插入元素会抛IllegalStateException:Queue full<br/>当阻塞队列空时，再往队列里remove移除元素会抛NoSuchElementException |
| -------- | ------------------------------------------------------------ |
| 特殊值   | 插入方法，成功ture失败false<br/>移除方法，成功返回出队列的元素，队列里没有就返回null |
| 一直阻塞 | 当阻塞队列满时，生产者线程继续往队列里put元素，队列会一直阻塞生产者线程直到put数据or响应中断退出<br/>当阻塞队列空时，消费者线程试图从队列里take元素，队列会一直阻塞消费者线程直到队列可用 |
| 超时退出 | 当阻塞队列满时，队列会阻塞生产者线程一定时间，超过限时后生产者线程会退出 |



**代码：**

```java
/**
 * BlockingQueueDemo
 *
 * @author daixl
 * @date 2021/6/20
 */
public class BlockingQueueDemo {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<String> blockingQueue=new ArrayBlockingQueue<>(3);

        /* 第一组：报异常
        System.out.println(blockingQueue.add("a"));
        System.out.println(blockingQueue.add("b"));
        System.out.println(blockingQueue.add("c"));
//        System.out.println(blockingQueue.add("d"));//多添加会报异常
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
//        System.out.println(blockingQueue.remove());//删除多会报异常
        */

        //第二组：false null
//        System.out.println(blockingQueue.offer("x"));
//        System.out.println(blockingQueue.offer("y"));
//        System.out.println(blockingQueue.offer("z"));
//        System.out.println(blockingQueue.offer("o"));//false
//
//        System.out.println(blockingQueue.poll());
//        System.out.println(blockingQueue.poll());
//        System.out.println(blockingQueue.poll());
//        System.out.println(blockingQueue.poll());//null



        //第三组：阻塞，无返回值
//        blockingQueue.put("a");
//        blockingQueue.put("a");
//        blockingQueue.put("a");
//        blockingQueue.put("a");//阻塞//卡住

//        System.out.println(blockingQueue.take());
//        System.out.println(blockingQueue.take());
//        System.out.println(blockingQueue.take());
//        System.out.println(blockingQueue.take());//卡住


        //第四组
        System.out.println(blockingQueue.offer("x"));
        System.out.println(blockingQueue.offer("y"));
        System.out.println(blockingQueue.offer("z"));
        System.out.println(blockingQueue.offer("a",3L, TimeUnit.SECONDS));//等待三秒，过时不候//false
    }
}
```



**TransferValue：**

```java
@NoArgsConstructor
@Getter
@Setter
public class Person {
    private Integer id;
    private String personname;

    public Person(String personname){
        this.personname=personname;
    }
}
```



```java
public class TestTransferValue {
    public void changeValue1(int age){
        age = 30;
    }
    public void changeValue2(Person person){
        person.setPersonName("xxx");
    }
    public void changeValue3(String str){
        str = "xxx";
    }

    public static void main(String[] args) {
        TestTransferValue test = new TestTransferValue();
        int age = 20;
        test.changeValue1(age);
        System.out.println("age----"+age);//20

        Person person = new Person("abc");
        test.changeValue2(person);
        System.out.println("personName-------"+person.getPersonName());//xxx

        String str = "abc";
        test.changeValue3(str);
        System.out.println("String-----"+str);//abc
    }
}
```



# ThreadPool线程池



## 01. 为什么用线程池

例子：
10年前单核CPU电脑，假的多线程，像马戏团小丑玩多个球，CPU需要来回切换。
现在是多核电脑，多个线程各自跑在独立的CPU上，不用切换效率高。

**线程池的优势：**
线程池做的工作只要是控制运行的线程数量，处理过程中将任务放入队列，然后在线程创建后启动这些任务，如果线程数量超过了最大数量，超出数量的线程排队等候，等其他线程执行完毕，再从队列中取出任务来执行。

**它的主要特点为：线程复用;控制最大并发数;管理线程**。

* 降低资源消耗。通过重复利用已创建的线程降低线程创建和销毁造成的销耗。
* 提高响应速度。当任务到达时，任务可以不需要等待线程创建就能立即执行。
* 提高线程的可管理性。线程是稀缺资源，如果无限制的创建，不仅会销耗系统资源，还会降低系统的稳定性，使用线程池可以进行统一的分配，调优和监控。



## 02. 线程池如何使用

**架构说明：**

​		Java中的线程池是通过Executor框架实现的，该框架中用到了Executor，Executors，ExecutorService，ThreadPoolExecutor这几个类。

![image-20210620034427946](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210620034427946.png)

**编码实现：**

* Executors.newFixedThreadPool(int)

```java
public static ExecutorService newFixedThreadPool(int nThreads) {
    return new ThreadPoolExecutor(nThreads, nThreads,
                                  0L, TimeUnit.MILLISECONDS,
                                  new LinkedBlockingQueue<Runnable>());
}
 
//newFixedThreadPool创建的线程池corePoolSize和maximumPoolSize值是相等的，它使用的是LinkedBlockingQueue
```



* Executors.newSingleThreadExecutor()

```java
public static ExecutorService newSingleThreadExecutor() {
    return new FinalizableDelegatedExecutorService
        (new ThreadPoolExecutor(1, 1,
                                0L, TimeUnit.MILLISECONDS,
                                new LinkedBlockingQueue<Runnable>()));
}
 
//newSingleThreadExecutor 创建的线程池corePoolSize和maximumPoolSize值都是1，它使用的是LinkedBlockingQueue
```



* Executors.newCachedThreadPool()

```java
public static ExecutorService newCachedThreadPool() {
    return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                                  60L, TimeUnit.SECONDS,
                                  new SynchronousQueue<Runnable>());
}
/*
newCachedThreadPool创建的线程池将corePoolSize设置为0，将maximumPoolSize设置为Integer.MAX_VALUE，它使用的是SynchronousQueue，也就是说来了任务就创建线程运行，当线程空闲超过60秒，就销毁线程。
*/
```

**代码：**

```java
/**
 * 线程池
 * Arrays
 * Collections
 * Executors
 */
public class MyThreadPoolDemo {

    public static void main(String[] args) {
        //List list = new ArrayList();
        //List list = Arrays.asList("a","b");
        //固定数的线程池，一池五线程

//       ExecutorService threadPool =  Executors.newFixedThreadPool(5); //一个银行网点，5个受理业务的窗口
//       ExecutorService threadPool =  Executors.newSingleThreadExecutor(); //一个银行网点，1个受理业务的窗口
       ExecutorService threadPool =  Executors.newCachedThreadPool(); //一个银行网点，可扩展受理业务的窗口

        //10个顾客请求
        try {
            for (int i = 1; i <=10; i++) {
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"\t 办理业务");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }

    }
}
```



**ThreadPoolExecutor底层原理;**

![image-20210620034743256](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210620034743256.png)

​		由此可见，三种创建线程池的方法都是调用ThreadPoolExecutor类，所以ThreadPoolExecutor才是创建线程池的核心类

## 03. ThreadPoolExecutor线程池几个重要参数---7个

**7大参数源码：**

```java
public ThreadPoolExecutor(int corePoolSize,
                          int maximumPoolSize,
                          long keepAliveTime,
                          TimeUnit unit,
                          BlockingQueue<Runnable> workQueue,
                          ThreadFactory threadFactory,
                          RejectedExecutionHandler handler) {
    if (corePoolSize < 0 ||
        maximumPoolSize <= 0 ||
        maximumPoolSize < corePoolSize ||
        keepAliveTime < 0)
        throw new IllegalArgumentException();
    if (workQueue == null || threadFactory == null || handler == null)
        throw new NullPointerException();
    this.corePoolSize = corePoolSize;
    this.maximumPoolSize = maximumPoolSize;
    this.workQueue = workQueue;
    this.keepAliveTime = unit.toNanos(keepAliveTime);
    this.threadFactory = threadFactory;
    this.handler = handler;
}
```

* corePoolSize：线程池中的常驻核心线程数；
* maximumPoolSize：线程池中能够容纳同时执行的最大线程数，此值必须大于等于1；
* keepAliveTime：多余的空闲线程的存活时间，当前池中线程数量超过corePoolSize时，当空闲时间，达到keepAliveTime时，多余线程会被销毁直到，只剩下corePoolSize个线程为止；
* unit：keepAliveTime的单位 ；
* workQueue：任务队列，被提交但尚未被执行的任务；
* threadFactory：表示生成线程池中工作线程的线程工厂，用于创建线程，一般默认的即可；
* handler：拒绝策略，表示当队列满了，并且工作线程大于，等于线程池的最大线程数（maximumPoolSize）时如何来拒绝，请求执行的runnable的策略。



![image-20210429101556277](https://img-blog.csdnimg.cn/img_convert/945f8ed60bc5d965a1a51a46b0e5c005.png)



## 04. 线程池底层工作原理

![image-20210429101702352](https://img-blog.csdnimg.cn/img_convert/ae4a4ae236f83d72cf90b7a469664ecc.png)

**银行demo演示：**

![image-20210429102340451](https://img-blog.csdnimg.cn/img_convert/388ba8ae4c2a1afc4684524af3fa2ab2.png)



**解释说明：**

1、在创建了线程池后，开始等待请求。
2、当调用execute()方法添加一个请求任务时，线程池会做出如下判断:
	2.1 如果正在运行的线程数量小于corePoolSize，那么马上创建线程运行这个任务;
	2.2 如果正在运行的线程数量大于或等于corePoo1lSize，那么将这个任务放入队列;
	2.3 如果这个时候队列满了且正在运行的线程数量还小于maximumPoolSize，那么还是要创建非核心线程立刻运行这个任务;
	2.4 如果队列满了且正在运行的线程数量大于或等于maximumPoolSize，那么线程池会启动饱和拒绝策略来执行。
3、当一个线程完成任务时，它会从队列中取下一个任务来执行。
4、当一个线程无事可做超过一定的时间（keepAliveTime）时，线程会判断:

## 05. 线程池用哪个？生产中如设置合理参数

**线程池的拒绝策略：**

**a. 是什么**

​		等待队列已经排满了，再也塞不下新任务了，同时，线程池中的max线程也达到了，无法继续为新任务服务。这个是时候我们就需要拒绝策略机制合理的处理这个问题。

**b. JDK内置的拒绝策略**

* AbortPolicy(默认)：直接抛出RejectedExecutionException异常阻止系统正常运行
* CallerRunsPolicy：“调用者运行”一种调节机制，该策略既不会抛弃任务，也不会抛出异常，而是将某些任务回退到调用者，从而降低新任务的流量。
* DiscardOldestPolicy：抛弃队列中等待最久的任务，然后把当前任务加人队列中，尝试再次提交当前任务。
* DiscardPolicy：该策略默默地丢弃无法处理的任务，不予任何处理也不抛出异常。如果允许任务丢失，这是最好的一种策略。

以上内置拒绝策略均实现了RejectedExecutionHandle接口



**在工作中单一的/固定数的/可变的三种创建线程池的方法哪个用的多？超级大坑：**

​		答案是一个都不用，我们工作中只能使用自定义的，

**Executors中JDK已经给你提供了，为什么不用？**

![image-20210620042631005](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210620042631005.png)



**在工作中如何使用线程池，是否自定义过线程池：**

```java
public class demo14 {
    public static void main(String[] args) {
        ExecutorService threadpool = new ThreadPoolExecutor(2,
                5,
                2L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(3),
                Executors.defaultThreadFactory(),
//                new ThreadPoolExecutor.AbortPolicy());//RejectedExecutionException//默认
//                new ThreadPoolExecutor.CallerRunsPolicy());//main 办理业务，回退给调用者
//                new ThreadPoolExecutor.DiscardPolicy());//抛弃，允许丢失
                new ThreadPoolExecutor.DiscardOldestPolicy());//抛弃等待最久的，允许丢失

        try {
            for (int i = 0; i <= 10; i++) { //10个客户
                threadpool.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + " 办理业务");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadpool.shutdown();
        }

    }
}
```



**扩展**

​		maximumPoolSize=CPU核数+1-------cpu密集型

```java
public class demo14 {
    public static void main(String[] args) {

        int t=Runtime.getRuntime().availableProcessors();

        ExecutorService threadpool = new ThreadPoolExecutor(
                2,
                t+1,
                2L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(3),
                Executors.defaultThreadFactory(),
//                new ThreadPoolExecutor.AbortPolicy());//RejectedExecutionException//默认
//                new ThreadPoolExecutor.CallerRunsPolicy());//main 办理业务，回退给调用者
//                new ThreadPoolExecutor.DiscardPolicy());//抛弃，允许丢失
                new ThreadPoolExecutor.DiscardOldestPolicy());//抛弃等待最久的，允许丢失

        try {
            for (int i = 0; i <= 10; i++) { //10个客户
                threadpool.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + " 办理业务");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadpool.shutdown();
        }

    }
}
```



# 十二、Java8之流式计算复习

## 01. **链式编程+流式计算**

![image-20210429120344203](https://img-blog.csdnimg.cn/img_convert/24398bb33c86c6ea038412ad0c93e4d2.png)



## 02. 四大函数式接口

Package java.util.function

![image-20210429120929026](https://img-blog.csdnimg.cn/img_convert/e39ef33ef7d6963b4b0b47b09784855c.png)

🛩 **函数式型接口 Function<T,R>**

​		传入一个参数，获取一个返回值

​		Function简单使用

```java
        Function<String,Integer> function=new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return 1024;
            }
        };
        System.out.println(function.apply("abc"));
```

lambda结合方法引用:

```java
//        Function<String,Integer> function=(String s)->{ return s.length(); };
        Function<String,Integer> function=s->{ return s.length(); };
        System.out.println(function.apply("abc"));
```



🛩 **断定型接口 Predicate<T>**

​		传入一个参数，返回一个 bool 值

```java
//        Predicate<String> predicate=new Predicate<String>() {
//            @Override
//            public boolean test(String s) {
//                return s.isEmpty();
//            }
//        }
        
        Predicate<String> predicate=s->{return s.isEmpty();};
        System.out.println(predicate.test("abc"));
```



🛩 **消费型接口 Consumer<T>**

​		传入参数，没有返回值

```java
        Consumer<String> consumer=new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };

        Consumer consumer1=s->{ System.out.println(s); };
        consumer1.accept("ssss");
```



🛩 **供给型接口 Supplier<T>**

​		没有传参数，有返回值

```java
        Supplier<String> supplier=new Supplier<String>() {
            @Override
            public String get() {
                return "java";
            }
        };
        Supplier<String> supplier1=()->{return "java";};
        System.out.println(supplier1.get());
```



## 03. 流式计算

**是什么：**

​		是数据渠道，用于操作数据源（集合、数组等）所生成的元素序列。“集合讲的是数据，流讲的是计算！”

**特点：**

* Stream 自己不会存储元素
* Stream 不会改变源对象。相反，他们会返回一个持有结果的新Stream。
* Stream 操作是延迟执行的。这意味着他们会等到需要结果的时候才执行。



**阶段：**

* 创建一个Stream：一个数据源（数组、集合）
* 中间操作：一个中间操作，处理数据源数据
* 终止操作：一个终止操作，执行中间操作链，产生结果



**代码：**

​		源头=>中间流水线=>结果

```
package com.atguigu.juc.study;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
class User
{
    private Integer id;
    private String  userName;
    private int     age;
}

/**
 * @create 2019-02-26 22:24
 *
 * 题目：请按照给出数据，找出同时满足
 *      偶数ID且年龄大于24且用户名转为大写且用户名字母倒排序
 *      最后只输出一个用户名字
 */
public class StreamDemo
{
    public static void main(String[] args)
    {
        User u1 = new User(11,"a",23);
        User u2 = new User(12,"b",24);
        User u3 = new User(13,"c",22);
        User u4 = new User(14,"d",28);
        User u5 = new User(16,"e",26);

        List<User> list = Arrays.asList(u1,u2,u3,u4,u5);

        list.stream().filter(p -> {
            return p.getId() % 2 == 0;
        }).filter(p -> {
            return p.getAge() > 24;
        }).map(f -> {
            return f.getUserName().toUpperCase();
        }).sorted((o1, o2) -> {
            return o2.compareTo(o1);
        }).limit(1).forEach(System.out::println);

    }
}
```



# 十三、分支合并框架

## 01. 原理

* Fork：把一个复杂任务进行分拆，大事化小
* Join：把分拆任务的结果进行合并 

![image-20210620210845038](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210620210845038.png)

![image-20210620210854330](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210620210854330.png)



**相关类：**

* ForkJoinPool

![image-20210620210929250](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210620210929250.png)

​		分支合并池    类比=>   线程池

* ForkJoinTask

![image-20210620211001602](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210620211001602.png)

​		ForkJoinTask    类比=>   FutureTask

* RecursiveTask

![image-20210620211033150](C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210620211033150.png)

```java
//递归任务：继承后可以实现递归(自己调自己)调用的任务
 
 class Fibonacci extends RecursiveTask<Integer> {
   final int n;
   Fibonacci(int n) { this.n = n; }
   Integer compute() {
     if (n <= 1)
       return n;
     Fibonacci f1 = new Fibonacci(n - 1);
     f1.fork();
     Fibonacci f2 = new Fibonacci(n - 2);
     return f2.compute() + f1.join();
   }
 }
```



**实例：**

```java
/**
 * 分支合并框架
 * ForkJoinPool
 * ForkJoinTask
 * RecursiveTask
 *
 * @author daixl
 * @date 2021/6/20
 */
public class ForkJoinDemo {

    public static void main(String[] args) throws Exception {
        MyTask myTask = new MyTask(0, 1000);
        ForkJoinPool threadPool = new ForkJoinPool();
        ForkJoinTask<Integer> forkJoinTask = threadPool.submit(myTask);
        System.out.println(forkJoinTask.get());
        threadPool.shutdown();
    }
}

class MyTask extends RecursiveTask<Integer> {

    private static final Integer ADJUST_VALUE = 10;
    private int begin;
    private int end;
    private int result;

    public MyTask(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    @Override
    protected Integer compute() {
        if ((end - begin) <= ADJUST_VALUE) {
            for (int i = begin; i <= end; i++) {
                result = result + i;
            }
        } else {
            int middle = (end + begin) / 2;
            MyTask task01 = new MyTask(begin, middle);
            MyTask task02 = new MyTask(middle + 1, end);
            task01.fork();
            task02.fork();
            result = task01.join() + task02.join();
        }
        return result;
    }
}
```



# 十四、异步回调



**原理：**

​		同步、异步、异步回调

<img src="C:\Users\dxl\AppData\Roaming\Typora\typora-user-images\image-20210620214842572.png" alt="image-20210620214842572" style="zoom:80%;" />

```java
import java.util.concurrent.CompletableFuture;

public class CompletableFutureDemo {

    public static void main(String[] args) throws Exception {
        //同步，异步，异步回调

        //同步
//        CompletableFuture<Void> completableFuture1 = CompletableFuture.runAsync(()->{
//            System.out.println(Thread.currentThread().getName()+"\t completableFuture1");
//        });
//        completableFuture1.get();

        //异步回调
        CompletableFuture<Integer> completableFuture2 = CompletableFuture.supplyAsync(()->{
            System.out.println(Thread.currentThread().getName()+"\t completableFuture2");
            int i = 10/0;
            return 1024;
        });

        completableFuture2.whenComplete((t,u)->{
            System.out.println("-------t="+t);
            System.out.println("-------u="+u);
        }).exceptionally(f->{
            System.out.println("-----exception:"+f.getMessage());
            return 444;
        }).get();

    }
}
```

























