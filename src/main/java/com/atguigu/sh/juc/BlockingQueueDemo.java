package com.atguigu.sh.juc;

import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * BlockingQueueDemo
 *
 * @author daixl
 * @date 2021/6/20
 */
public class BlockingQueueDemo {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<String> blockingQueue=new ArrayBlockingQueue<>(3);

        /* 第一组：报异常
        System.out.println(blockingQueue.add("a"));
        System.out.println(blockingQueue.add("b"));
        System.out.println(blockingQueue.add("c"));
//        System.out.println(blockingQueue.add("d"));//多添加会报异常
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
//        System.out.println(blockingQueue.remove());//删除多会报异常
        */

        //第二组：false null
//        System.out.println(blockingQueue.offer("x"));
//        System.out.println(blockingQueue.offer("y"));
//        System.out.println(blockingQueue.offer("z"));
//        System.out.println(blockingQueue.offer("o"));//false
//
//        System.out.println(blockingQueue.poll());
//        System.out.println(blockingQueue.poll());
//        System.out.println(blockingQueue.poll());
//        System.out.println(blockingQueue.poll());//null



        //第三组：阻塞，无返回值
//        blockingQueue.put("a");
//        blockingQueue.put("a");
//        blockingQueue.put("a");
//        blockingQueue.put("a");//阻塞//卡住

//        System.out.println(blockingQueue.take());
//        System.out.println(blockingQueue.take());
//        System.out.println(blockingQueue.take());
//        System.out.println(blockingQueue.take());//卡住


        //第四组
        System.out.println(blockingQueue.offer("x"));
        System.out.println(blockingQueue.offer("y"));
        System.out.println(blockingQueue.offer("z"));
        System.out.println(blockingQueue.offer("a",3L, TimeUnit.SECONDS));//等待三秒，过时不候//false
    }
}
