package com.atguigu.sh.juc;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * JAVA多线程实现的三种方式
 *
 * @author daixl
 * @date 2021/6/17
 */
public class CallableDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask futureTask = new FutureTask(new MyThread());
        new Thread(futureTask, "A").start();
        new Thread(futureTask, "B").start();
        System.out.println(Thread.currentThread().getName() + "  计算完成");
        System.out.println(futureTask.get());

    }
}

class MyThread implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println("come in here");
        //暂停一会儿线程
        try { TimeUnit.SECONDS.sleep( 4 ); } catch (InterruptedException e) { e.printStackTrace(); }
        return 1024;
    }
}
