package com.atguigu.sh.juc;

/**
 * Lambda表达式
 * 2.lambda expression
 * 2.1 口诀：
 *      拷贝小括号，写死右箭头，落地大括号
 * 2.2 @FunctionalInterface
 * 2.3 default
 * 2.4 静态方法实现
 *
 * @author daixl
 * @date 2021/6/14
 */
public class LambdaExpressDemo {

    public static void main(String[] args) {

        /**
         * 使用匿名内部类实现接口
         */
        Foo foo = new Foo() {
            @Override
            public void sayHello() {
                System.out.println("hello world");
            }
        };
        foo.sayHello();

        /**
         * 使用lambda表达式
         */
        Foo foo1 = () -> { System.out.println("hello world lambda"); };
        foo1.sayHello();

        Foo2 foo2 = (x, y) -> {
            System.out.println("come in here");
            return x + y;
        };
        System.out.println(foo2.add(2, 5));
        System.out.println(foo2.div(4, 2));
        System.out.println(Foo2.mv(2, 3));
    }
}

/**
 * 函数式接口，即接口中有且仅有一个方法，此时可以使用lambda表达式去实现该接口下的方法
 */
@FunctionalInterface
interface Foo {

    public void sayHello();
}

@FunctionalInterface
interface Foo2 {

    public int add(int x, int y);

    //default方法，java8以后接口是可以实现方法的，即为default方法
    default int div(int x, int y) {
        return x / y;
    }

    public static int mv(int x, int y) {
        return x * y;
    }

}
