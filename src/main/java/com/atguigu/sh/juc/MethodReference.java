package com.atguigu.sh.juc;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 方法引用：lambda表达式的主体仅包含一个表达式，且该表达式仅调用了一个已经存在的方法，我们就可以使用方法引用的方式直接调用
 *
 * @author daixl
 * @date 2021/6/21
 */
public class MethodReference {

    public static void main(String[] args) {
        //1.对象::实例方法名
        Consumer<String> consumer = str -> {System.out.println(str);};
        consumer.accept("yyds");
        Consumer<String> consumer1 = System.out::println;
        consumer1.accept("yyds2");

        //2.类::静态方法名
        Function<String, Integer> function = str -> {return Integer.parseInt(str);};
        System.out.println(function.apply("123456"));
        Function<String, Integer> function1 = Integer::parseInt;
        System.out.println(function1.apply("789654"));

        //3.类::实例方法名（特殊）
        //注意：当lambda体的第一个参数是实例方法的调用者，另一个参数就是实例方法的参数时，可以使用类::实例方法名调用
        BiFunction<String, String, Boolean> biFunction = (a, b) -> {return a.equals(b);};
        System.out.println(biFunction.apply("a", "b"));
        BiFunction<String, String, Boolean> biFunction1 = String::equals;
        System.out.println(biFunction.apply("a", "a"));

        //构造器应用，
        //语法：类::new
        Supplier<String> supplier = () -> {return new String();};
        Supplier<String> supplier1 = String::new;


        //数组引用
        //语法：数组::new
        Function<Integer, String[]> function2 = a -> {return new String[a];};
        Function<Integer, String[]> function3 = String[]::new;
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.forEach(System.out::println);
    }
}
