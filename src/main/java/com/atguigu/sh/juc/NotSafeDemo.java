package com.atguigu.sh.juc;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 题目：请举例说明集合类是不安全的
 * 1    故障现象
 *      java.util.ConcurrentModificationException
 * 2    导致原因
 *      线程不安全
 * 3    解决方法
 *   3.1 Vector
 *   3.2 Collections.synchronizedList(new ArrayList<>())
 *   3.3 new CopyOnWriteArrayList<>()
 *
 * @author daixl
 * @date 2021/6/16
 */
public class NotSafeDemo {

    public static void main(String[] args) {
        List<String> list = new CopyOnWriteArrayList<>();//Collections.synchronizedList(new ArrayList<>());//new Vector<>();//new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                list.add(UUID.randomUUID().toString().substring(0, 8));
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }
}
