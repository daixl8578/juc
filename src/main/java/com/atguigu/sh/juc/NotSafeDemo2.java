package com.atguigu.sh.juc;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 题目：请举例说明集合类是不安全的
 * 1    故障现象
 *      java.util.ConcurrentModificationException
 * 2    导致原因
 *      线程不安全
 * 3    解决方法
 *   3.1 Collections.synchronizedSet(new HashSet<String>())
 *   3.2 new CopyOnWriteArraySet<>()
 *
 * @author daixl
 * @date 2021/6/17
 */
public class NotSafeDemo2 {

    public static void main(String[] args) {
        Set<String> set = new CopyOnWriteArraySet<>();//Collections.synchronizedSet(new HashSet<String>());//new HashSet<String>();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                set.add(UUID.randomUUID().toString().substring(0, 8));
                System.out.println(set);
            }, String.valueOf(i)).start();
        }
    }
}
