package com.atguigu.sh.juc;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 题目：请举例说明集合类是不安全的
 * 1    故障现象
 *      java.util.ConcurrentModificationException
 * 2    导致原因
 *      线程不安全
 * 3    解决方法
 *   3.1 Collections.synchronizedMap(new HashMap<>())
 *   3.2 new ConcurrentHashMap<>()
 *
 * @author daixl
 * @date 2021/6/17
 */
public class NotSafeDemo3 {

    public static void main(String[] args) {
        Map<String, String> map = new ConcurrentHashMap<>();//Collections.synchronizedMap(new HashMap<>());//new HashMap<>();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0, 8));
                System.out.println(map);
            }, String.valueOf(i)).start();
        }
    }
}
