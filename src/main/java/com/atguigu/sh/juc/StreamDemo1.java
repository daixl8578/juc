package com.atguigu.sh.juc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 /**
 * 题目：请按照给出数据，找出同时满足
 *      偶数ID且年龄大于24且用户名转为大写且用户名字母倒排序
 *      最后只输出一个用户名字
 *
 * @author daixl
 * @date 2021/6/20
 */
public class StreamDemo1 {
    public static void main(String[] args) {

        User u1=new User(11,"A",23);
        User u2=new User(12,"B",24);
        User u3=new User(13,"C",22);
        User u4=new User(14,"D",28);
        User u5=new User(16,"E",27);

        List<User> list= Arrays.asList(u1,u2,u3,u4,u5);
        list.stream().filter(user -> {return user.getId() % 2 == 0;})
                .filter(user -> {return user.getAge() > 24;})
                .map(user -> {return user.getName().toUpperCase();})
                .sorted((o1, o2) -> {return o2.compareTo(o1);})
                .limit(1)
                .forEach(System.out::println);

        //四大函数型接口
        /**
        Function<String, Integer> function = str -> {return str.length();};
        System.out.println(function.apply("abc"));

        Predicate<String> predicate = s -> {return s.isEmpty();};
        System.out.println(predicate.test("11"));

        Consumer<String> consumer = str -> {System.out.println(str);};
        consumer.accept("ssss");

        Supplier<String> supplier = () -> {return "Supplier";};
        System.out.println(supplier.get());
         */
    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class User {

    private int id;
    private String name;
    private int age;

}