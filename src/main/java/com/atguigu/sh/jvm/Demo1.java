package com.atguigu.sh.jvm;

/**
 * Demo1
 *
 * @author daixl
 * @date 2021/6/22
 */
public class Demo1 {

    public static void main(String[] args) {
        //查看电脑cpu核数
        System.out.println(Runtime.getRuntime().availableProcessors());
        // 返回 Java 虚拟机试图使用的最大内存量
        long maxMemory = Runtime.getRuntime().maxMemory();
        System.out.println("MAX_MEMORY = " + maxMemory + "（字节）、" + (maxMemory / (double) 1024 / 1024) + "MB");
        // 返回 Java 虚拟机中的内存总量
        long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("TOTAL_MEMORY = " + totalMemory + "（字节）、" + (totalMemory / (double) 1024 / 1024) + "MB");
    }
}
