package com.atguigu.sh.jvm;

import java.util.concurrent.TimeUnit;

/**
 * JMM：java内存模型
 * 静态的（一次）>构造块>构造方法
 *
 * @author daixl
 * @date 2021/6/26
 */
public class Jmm {

    public static void main(String[] args) {
        MyNumber myNumber = new MyNumber();

        new Thread(() -> {
            //暂停一会儿线程
            try { TimeUnit.SECONDS.sleep( 3 ); } catch (InterruptedException e) { e.printStackTrace(); }
            myNumber.addTo1205();
            System.out.println(Thread.currentThread().getName() + "\t " + myNumber.number);
        }, "AAA").start();

        System.out.println(myNumber.number);
        while (myNumber.number == 10) {
            //需要一种通知机制通知main线程，number已经被修改为1205，跳出while循环
        }
        System.out.println(Thread.currentThread().getName() + "\t 主线程结束");
    }
}

class MyNumber{

    public int number = 10;

    public void addTo1205(){
        this.number = 1205;
    }
}